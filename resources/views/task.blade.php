@extends('layout')

@section('toto', 'task')

@section('sidebar')
    @parent

@endsection

@section('content')
<style>
body{background-color:orange;
text-align: center;}

</style>



<a href="/todo/create">ADD TASK</a>

<table border=1>

  <tr>
    <th>TASK NO.</th>
    <th>TASK NAME</th>
    <th>DESCRIPTION</th>
    <th>STATUS</th>
    <th>EDIT</th>
    <th>DELETE</th></tr>

    @foreach ($data as $row)
      <tr> <td> {{ $row->id }}</td>
    <td>    {{ $row->taskname }}</td>
      <td>  {{ $row->description }}</td>

    <td> @php
      if ($row->status==0)  echo "TODO";
      elseif  ($row->status==1) echo "DONE";

       @endphp
     </td>
    <td><a href="/todo/{{$row->id}}/edit">EDIT</a></td>

    <form method='post' action="/todo/{{$row->id}}">
      @csrf
      @method("DELETE")
    <td> <input type="submit" value="delete"> </td>
  </form>
   </tr>


  @endforeach




</table>


@endsection
