@extends('layout')

@section('toto', 'task')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
<form method="post" action="/todo/{{$var->id}}">
  @csrf 
  @method("PUT")
  TASK NAME <input type="text" name="taskname" value="{{$var->taskname}}">
    <br>

 DESCRIPTION  <input type="text" name="taskdescription" value="{{$var->description}}">
  <br>

  STATUS <input type="radio" name="status" id="yes"   @if ($var->status==1) checked @endif value="1">yes
         <input type="radio" name="status" id="no"  @if($var->status==0)  checked  @endif value="0">no <br>

<input type="submit" value="SUMBIT">
</form>
@endsection
