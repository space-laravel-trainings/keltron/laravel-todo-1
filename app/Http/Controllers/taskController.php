<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
class taskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $data=Task::all();
    //  return $data;

      return view ('task',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('addtask');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $a=$request->taskname;
      $b=$request->taskdescription;
      $c=$request->status;

      $obj=new Task();
      $obj->taskname=$a;
      $obj->description=$b;
      $obj->status=$c;
      $obj->save();

      return redirect('/todo');
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $var=Task::find($id);
        return view ('/edit',compact('var'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $a=$request->taskname;
      $b=$request->taskdescription;
      $c=$request->status;

      $array=["taskname"=>$a,"description"=>$b,"status"=>$c];
      $var=Task::find($id);
      $var->update($array);

      return redirect('/todo');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $var=Task::find($id);
        $var->delete();
        return redirect('/todo');

    }
}
